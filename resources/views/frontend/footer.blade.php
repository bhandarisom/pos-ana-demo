 <footer class="my-5 pt-5 text-muted text-center text-small">
    <p class="mb-1">&copy; {{date('Y')}} {{ config('app.name') }}</p>
  </footer>