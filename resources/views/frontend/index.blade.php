@extends('frontend.main')
@section('title','Pos Index')
@section('custom_js')
<script type="text/javascript">
   $(document).ready(function () {
            $('#select_product').on('change', function (e) {
                e.preventDefault();
                var slug = $(this).val();
                if (slug) {
                    $.ajax({
                        url: baseUrl + '/select_product',
                        url: "{{ url('select_product') }}",
                        type: "POST",
                        data: {
                        slug: slug,
                        },
                        dataType: "json",
                        success: function (data) {
                            console.log(data.product.previous_price);
                            if(data.type == 'success'){
                            $('#price').val(data.product.previous_price);
                          }else{
                            $('#price').val("");
                              swal({
                               position: 'top-end',
                                  type: 'info',
                                  title: 'Select the Product To Added',
                                  showConfirmButton: false,
                                  timer: 1500,
                                  animation: false,
                                  customClass: 'animated data'
                            }); 
                           }
                        }
                    });
                }else{
                  $('#price').val("");
                }
            });

             $(".addto-cart").click(function(e){
                e.preventDefault();
                var product = document.getElementById('select_product').value;
                var quantity = document.getElementById('quantity_id').value;
                if(product){
                    $.ajax({
                    type: "POST",
                    url: baseUrl + "/products/" + product + '/cart',
                    datatype: "json",
                    data: {
                        product: product,
                        quantity: quantity,
                        },
                        success: function (data) {
                            console.log(data);
                            // window.location = data.url;
                            $('#cart_form').empty(); 
                            $('#cart_form').append(data.productlist); 
                        }
                    });
                  }else{
                      swal({
                                position: 'top-end',
                                type: 'info',
                                title: 'Please Select Product',
                                showConfirmButton: false,
                                timer: 1500,
                                animation: false,
                                customClass: 'animated data'
                            });
                  }
            });

              $(".order_checkout").click(function(e){
                e.preventDefault();
                    $.ajax({
                    type: "POST",
                    url: baseUrl + '/orderProducts',
                    datatype: "json",
                        success: function (data) {
                            console.log(data);
                            if(data.type == 'success'){
                            window.open(data.url, '_blank '); 
                          }else{
                            swal({
                              position: 'top-end',
                                  type: 'info',
                                  title: 'Add the Products Before checkout',
                                  showConfirmButton: false,
                                  timer: 1500,
                                  animation: false,
                                  customClass: 'animated data'
                            });
                          }
                        }
                    });
            });

              $(".reset_cart").click(function(e){
                e.preventDefault();
                    $.ajax({
                    type: "POST",
                    url: baseUrl + '/resetCart',
                    datatype: "json",
                        success: function (data) {
                            console.log(data);
                            // window.location = data.url;
                            $('#cart_form').empty(); 
                            $('#cart_form').append(data.productlist);
                             swal({
                              position: 'top-end',
                                  type: 'info',
                                  title: 'Cart have been reset',
                                  showConfirmButton: false,
                                  timer: 1500,
                                  animation: false,
                                  customClass: 'animated data'
                            });
                        }
                    });
            });
        });
</script>
@endsection
@section('dynamicData')

<div class="container">
  <main>
    <div class="py-5 text-center">
     <!--  <img class="d-block mx-auto mb-4" src="../assets/brand/bootstrap-logo.svg" alt="" width="72" height="57"> -->
      <h2>Checkout</h2>
      <p class="lead">Add the products below and submit to checkout the transactions after completing it.</p>
      <button class="w-100 btn btn-secondary reset_cart" >Reset Data For New Customer</button>
    </div>

    <div class="row g-3">
         <div class="col-md-12 col-lg-12">
        <!-- <h4 class="mb-3">Add Products</h4> -->
        <form id="myaddform">
          <div class="row g-3">
            <div class="col-sm-3">
              <label for="ProductName"  class="form-label">Select Product</label>
               <select class="form-select  mb-3" aria-label=".form-select-lg example" name = "product_slug" id="select_product" required>
                  <option value="">Select Product</option>
                  @foreach($products as $product)
                  <option value="{{$product->slug}}">{{$product->title}}</option>
                  @endforeach
              </select>
              <div class="invalid-feedback">
                Product name is required.
              </div>
            </div>

            <div class="col-sm-3">
              <label for="numberCount" class="form-label">Number</label>
              <input type="number" class="form-control" id="quantity_id"  placeholder="" value="{{ old('quantity') ?? "1" }}" min="1" required>
              <div class="invalid-feedback">
                Total Number is required.
              </div>
            </div>

             <div class="col-sm-3">
              <label for="price" class="form-label">Price</label>
              <!-- <div id="select_price"></div> -->
              <input type="text" class="form-control" id="price" placeholder="" value="" readonly>
              <div class="invalid-feedback">
                Valid Price name is required.
              </div>
            </div>

             <!-- <div class="col-sm-3 add_to_cart" style="display: none";> -->
             <div class="col-sm-3 add_to_cart" >
              <label for="Submit" class="form-label">Add to Cart</label>
              <button class="w-100 btn btn-primary addto-cart" >Continue</button>
            </div>
          </div>
            {{csrf_field()}}
        </form>
      </div>
      <div class="col-md-12 col-lg-12  order-md-last">
        <div id="cart_form">
          <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted"> @if(count($selectproducts) != null) Your cart @else Add Products @endif</span>
            <span class="badge bg-secondary rounded-pill">{{count($selectproducts) ?? ""}}</span>
          </h4>
          <ul class="list-group mb-3">
           @if(count($selectproducts) != null)    
         @foreach($selectproducts as $product)
            @php
                $quantity = $carts[$product->id]['quantity'];
                $unitPrice = $carts[$product->id]['unit_price'];
                $linePrice = $carts[$product->id]['line_price'];
            @endphp
            <li class="list-group-item d-flex justify-content-between lh-sm">
                <div>
                  <h6 class="my-0">Product name : {!! $product->title !!}</h6>
                  <small class="text-muted">Total Number : {!! $quantity !!}</small>
                  <small class="text-muted">Unit Price : {!!$unitPrice!!}</small>
                </div>
                <span class="text-muted">{!!$linePrice!!}</span>
            </li>
         @endforeach
          <li class="list-group-item d-flex justify-content-between bg-light">
            <div class="text-success">
              <h6 class="my-0">Discount</h6>
              <small></small>
            </div>
            <span class="text-success">−{{$totalCartPrice-$totalDiscountCartPrice}}</span>
          </li>
          <li class="list-group-item d-flex justify-content-between">
            <span>Total (Rs)</span>
            <strong> {!! $totalDiscountCartPrice !!} </strong>
          </li>
           @endif  
         </ul>
        </div>
        <div class="col-sm-3">
              <!-- <form method="post" action="{{route('order.store')}}"> -->
                <button class="w-100 btn btn-primary order_checkout" value="submit">Checkout</button>
              <!-- {{csrf_field()}} -->
              <!-- </form> -->
       </div>
      </div>
    </div>
  </main>

 @include('frontend.footer')
</div>
@endsection