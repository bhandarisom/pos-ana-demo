<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>{!! config('app.name') !!}</title>
  <style>
    body {
      background: #fff;
      margin: 0;
      padding: 0;
      color: #111;
      font-family: Arial, Helvetica, sans-serif;
      font-size: 12px;
      line-height: 1.4
    }
    page {

      display: block;
      margin: 0 auto;
      position: relative
    }
    page[size="A4"] {
      width: 21cm;
      height: 29.7cm;
    }
    .pd {
      padding: 30px 10px ;
    }
    table {
      width: 100%;
      border-collapse: collapse;
    }
    table td {
      padding: 5px 10px;
      border: 1px solid #333;
    }
    .logo {
      text-align: center;
      margin-bottom: 30px
    }
    .heading {
      font-size: 22px;
      color: #19477B;
      margin-bottom: 25px;
    }
    @media print {
      body, page {
        margin: 0;
        box-shadow: 0;
      }
    }
  </style>
</head>

<body style="padding: 0;">

  <div class="pd" >
    <!-- <div class="logo"><img src="images/logo.png" width="450px;"></div> -->
    <div class="heading">Print PDF
  <div style="float: right;font-size: 14px;"> Date : &nbsp;{{date('d M Y')}}</div>
    </div>
    <table>
       <tr>
        <td width="50%"><strong>Product name </strong></td>
        <td width="50%"><strong>Price </strong></td>
      </tr>
       @if(count($selectproducts) != null)    
         @foreach($selectproducts as $product)
            @php
                $quantity = $carts[$product->id]['quantity'];
                $unitPrice = $carts[$product->id]['unit_price'];
                $linePrice = $carts[$product->id]['line_price'];
            @endphp
      <tr>
        <td width="50%">{!! $product->title !!} &nbsp;{!! $quantity !!} *{!!$unitPrice!!}</td>
        <td width="50%">{!!$linePrice!!}</td>
      </tr>
      @endforeach
      @endif
      <tr>
        <td ><strong>Total: &nbsp;</strong></td>
        <td ><strong>{{$totalCartPrice}}&nbsp;</strong></td>
      </tr>
      <tr>
        <td ><strong>Discount: &nbsp;</strong></td>
        <td ><strong>{{$totalCartPrice-$totalDiscountCartPrice}} &nbsp;</strong></td>
      </tr>
      <tr>
         <td ><strong>Final Total (Rs) &nbsp;</strong></td>
        <td ><strong>{!! $totalDiscountCartPrice !!} &nbsp;</strong></td>
      </tr>

    </table>

    <br>
    <br>
    <div><strong>Check Out: &nbsp;</strong>Counter 1</div>
    <br/>
    <div style="position:absolute;bottom:-30px;">Thank You Very Much For Shopping With Us...</div>

  </div>

</body>
</html>
