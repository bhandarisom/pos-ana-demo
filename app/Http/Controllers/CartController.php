<?php

namespace App\Http\Controllers;
use App\Repositories\ProductRepository;
use App\Classes\FileHelper;
use Illuminate\Http\Request;
use Session;



class CartController extends Controller
{
    /**
     * CategoryController constructor.
     *
     * @param CategoryRepository $categories
     */
    public function __construct(
        ProductRepository $products
    ){
         $this->products = $products;
         $this->fileHelper = new FileHelper();
    }

    /**
     * Show the application home page.
     *
     * @return \Illuminate\Http\Response
     */

   
    public function store(Request $request,$slug)
    {
         $product = $this->products->findByField('slug', $slug);
        if ($product) {
            $quantity = $request->quantity ? : 1;
            $unitPrice = $product->previous_price;
            $unitDiscountPrice = $product->amount;
            $discountPrice = $quantity*$unitDiscountPrice;
            $linePrice = $quantity*$unitPrice;
            $cartArray = session()->get('cartProducts') ? : [];
            $cartArray[$product->id] = [
                'quantity'=>$quantity,
                'unit_price'=>$unitPrice,
                'line_price'=>$linePrice,
                'discountPrice' => $discountPrice
            ];
            session()->put('cartProducts', $cartArray);


            $cartProducts = array_keys($cartArray);
            $totalCartPrice = array_sum(array_column($cartArray, 'line_price'));
            $totalDiscountCartPrice = array_sum(array_column($cartArray, 'discountPrice'));
            $selectproducts = $this->products->select(['*'])
                ->whereIn('id', $cartProducts)
                ->get();
            $productlist = $this->fileHelper->get_product_list($selectproducts,$cartArray,$totalCartPrice,$totalDiscountCartPrice);

            return response()->json([
                // 'url'=>url('/'),
                'type' => 'success',
                'productlist' => $productlist,
            ], 200);
        }
        return response()->json([
            'type' => 'error',
        ], 422);
    }

    public function resetCart()
    {
        session()->forget(['cartProducts']);
        return response()->json([
                'type' => 'success'
            ], 200); 
    }


  
}
