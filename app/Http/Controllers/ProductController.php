<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductRepository;
use Session;


class ProductController extends Controller
{
   /**
     * ProductController constructor.
     *
    
     */
    public function __construct(ProductCategoryRepository $categories,
                                ProductRepository $products
                              )
    {
       $this->categories = $categories;
       $this->products = $products;
    }

    /**
     * Show the application product detail.
     *
     * @return \Illuminate\Http\Response
     */
    public function select_product(Request $request)
    {
      if($this->products->findByField('slug',$request->slug)){
      return response()->json([
                'type' => 'success',
                'product' => $this->products->findByField('slug',$request->slug),
            ], 200);
    }else{
      return response()->json([
                'type' => 'error',
            ], 200);
    }
    } 

    
}

