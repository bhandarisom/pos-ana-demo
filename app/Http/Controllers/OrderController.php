<?php

namespace App\Http\Controllers;

use App\Repositories\OrderRepository;
use App\Repositories\ProductRepository;
use App\Requests\Order\StoreRequest;
use Illuminate\Http\Request;
use PDF;



class OrderController extends Controller
{
    /**
     * CheckoutController constructor.
     *
     * @param ProductRepository $products
     * @param OrderRepository $orders
     */
    public function __construct(
        ProductRepository $products,
        OrderRepository $orders
    )
    {
        $this->products = $products;
        $this->orders = $orders;
    }

    /**
     * Store a new order in storage.
     *
     * @return mixed
     */
    public function store(Request $request)
    {
        $carts = session()->get('cartProducts') ? : [];
       if($carts != null){
            $totalCartPrice = array_sum(array_column($carts, 'line_price'));
            $totalDiscountedCartPrice = array_sum(array_column($carts, 'discountPrice'));
            $orderData['number'] = time();
            $orderData['sub_total'] = $totalCartPrice;
            $orderData['total_price'] = $totalCartPrice;
            $orderData['discount_amount'] = $totalCartPrice-$totalDiscountedCartPrice;
             $data = [
                'orderData'=>$orderData,
                'productData'=>$this->getProductData(),
            ];
            $order = $this->orders->create($data);
            if($order){
           return response()->json([
                'url'=>url('orderPrint'),
                'type' => 'success',
            ], 200);
           }else{
                return response()->json([
                'type' => 'error',
            ]);
           }
       }else{
           return response()->json([
                'type' => 'error',
            ]);
       }
        

    }

      protected function getProductData()
    {
        $carts = session()->get('cartProducts') ? : [];
        $productData = [];
        $products = $this->products->select(['*'])
            ->whereIn('id', array_keys($carts))
            ->get();
        foreach($products as $product)
        {
            $productData[] = [
                'product_id'=>$product->id,
                'product_name'=>$product->title,
                'unit_price'=>$carts[$product->id]['unit_price'],
                'quantity'=>(int)$carts[$product->id]['quantity'],
                'sub_total'=>$carts[$product->id]['line_price'],
            ];
        }
        return $productData;
    }

    public function print()
    {
        $carts = session()->get('cartProducts') ? : [];
        $cartProducts = array_keys($carts);
        $selectproducts = $this->products->select(['*'])
            ->whereIn('id', $cartProducts)
            ->get();
        $totalCartPrice = array_sum(array_column($carts, 'line_price'));
        $totalDiscountCartPrice = array_sum(array_column($carts, 'discountPrice'));    
        $pdf = PDF::loadView('frontend.view_cart',compact('selectproducts','carts','totalCartPrice','totalDiscountCartPrice'));
       
        return $pdf->stream();
    }
  
}
