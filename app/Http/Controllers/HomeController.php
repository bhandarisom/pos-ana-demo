<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductRepository;


class HomeController extends Controller
{
    public function __construct(ProductCategoryRepository $categories,
                                ProductRepository $products
                              )
    {
       $this->categories = $categories;
       $this->products = $products;
    }
    /**
     * HomeController constructor.
     *
    
     */


    /**
     * Show the application home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $carts = session()->get('cartProducts') ? : [];
        $cartProducts = array_keys($carts);
        $totalCartPrice = array_sum(array_column($carts, 'line_price'));
        $totalDiscountCartPrice = array_sum(array_column($carts, 'discountPrice'));
        $selectproducts = $this->products->select(['*'])
            ->whereIn('id', $cartProducts)
            ->get();

        return view('frontend.index')
            ->withProducts($this->products->get())
            ->withSelectproducts($selectproducts)
            ->withTotalCartPrice($totalCartPrice)
            ->withTotalDiscountCartPrice($totalDiscountCartPrice)
            ->withCarts(session()->get('cartProducts'));
    }      
}

