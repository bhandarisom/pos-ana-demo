<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number',
        'payment_id',
        'notes',
        'discount_amount',
        'sub_total',
        'total_price',
        'payment_status',
        'is_active'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

   
    public function products()
    {
        return $this->hasMany(OrderProduct::class, 'order_id');
    }


    public function getSubTotal()
    {
        return 'Rs.'.number_format($this->sub_total, 2);
    }

    
    public function getTotalAmount()
    {
        return 'Rs.'.number_format($this->total_price, 2);
    }

    public function getCreatedAt()
    {
        return $this->created_at->toFormattedDateString();
    }

    public function getPaymentId()
    {
        return $this->payment_id;
    }

   
}
