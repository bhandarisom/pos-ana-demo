<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name',
        'email_address',
        'mobile_no',
        'country_code',
        'date_of_birth',
        'address',
        'attachment',
        'state',
        'pan_no',
        'council_no',
        'educational_background',
        'city',
        'postal_code',
        'password',
        'role_id',
        'is_active',
        'verify_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the audit logs for the user.
     */
    public function logs()
    {
        return $this->morphMany(AuditLog::class, 'loginable');
    }

    

    public function role()
    {
        return $this->belongsTo(Role::class,'role_id');
    }

    public function getFullName()
    {
        return $this->full_name;
    }

         public function getCreatedAt()
    {
        return $this->created_at ? $this->created_at->format('M , Y ') : "";
    }

      public function userrating()
    {
        return $this->hasOne('App\Models\Rating','user_id');
    }

       public function userproductrating()
    {
        return $this->hasOne('App\Models\ProductRating','user_id');
    }
 
}
