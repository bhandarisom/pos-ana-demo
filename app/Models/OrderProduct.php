<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'product_id',
        'product_name',
        'unit_price',
        'quantity',
        'sub_total'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function getUnitPrice()
    {
        return 'Rs.'.number_format($this->unit_price, 2);
    }

    public function getSubTotal()
    {
        return 'Rs.'.number_format($this->sub_total, 2);
    }

    public function getProductName()
    {
        return $this->product_name;
    }

 

}
