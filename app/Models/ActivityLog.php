<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'activity_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'loginable_id',
        'loginable_type',
        'ip_address',
        'route',
        'agent',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get all of the owning loginable models.
     */
    public function loginable()
    {
        return $this->morphTo();
    }

    public function getCreatedAt()
    {
        return $this->created_at->format('M d, Y h:i A');
    }
}
