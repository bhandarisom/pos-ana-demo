<?php

namespace App\Models;

use App\Traits\ModelEventLogger;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

class Product extends Model
{
    use ModelEventLogger,Sluggable,SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'amount',
        'slug',
        'attachment',
        'description',
        'category_id',
        'is_active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

      public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Get the admin that owns the category.
     */

          public function category()
            {
                return $this->belongsTo(ProductCategory::class, 'category_id');
            }

        public function getCreatedAt()
            {
                return $this->created_at ? $this->created_at->format('M d, Y h:i A') : "";
            }

           public function orderproduct()
            {
                return $this->hasMany(OrderProduct::class, 'product_id');
            }

               public function images()
            {
                return $this->hasMany(ProductImages::class,'product_id');
            }

            

}
