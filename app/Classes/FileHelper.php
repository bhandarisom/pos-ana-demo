<?php 
namespace App\Classes;


class FileHelper
{
      public function get_product_list($selectproducts,$cartArray,$totalCartPrice,$totalDiscountCartPrice)
    {
         $returnList = '<h4 class="d-flex justify-content-between align-items-center mb-3">
                        <span class="text-muted">  Your cart </span>
                        <span class="badge bg-secondary rounded-pill">'. count($selectproducts) .'</span>
                      </h4>';
        $returnList .= '<ul class="list-group mb-3">';
        foreach ($selectproducts as $key => $product) {
                $quantity = $cartArray[$product->id]['quantity'];
                $unitPrice = $cartArray[$product->id]['unit_price'];
                $linePrice = $cartArray[$product->id]['line_price'];

        $returnList .= '<li class="list-group-item d-flex justify-content-between lh-sm"><div><h6 class="my-0">Product name : ' . $product->title . '</h6><small class="text-muted">Total Number :' . $quantity . '</small><small class="text-muted">Unit Price :' . $unitPrice . ' </small></div><span class="text-muted">' . $linePrice . '</span></li>';
        }
        $returnList .=  '<li class="list-group-item d-flex justify-content-between bg-light">
            <div class="text-success">
              <h6 class="my-0">Discount</h6>
              <small></small>
            </div>
            <span class="text-success">− '. ($totalCartPrice-$totalDiscountCartPrice) .' </span>
          </li>
          <li class="list-group-item d-flex justify-content-between">
            <span>Total (Rs)</span>
            <strong> '. $totalDiscountCartPrice .' </strong>
          </li>';
        $returnList .= '</ul>';
        return $returnList;
    }
}