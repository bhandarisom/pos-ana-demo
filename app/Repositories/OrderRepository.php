<?php

namespace App\Repositories;

use App\Models\Order;
use DB;

class OrderRepository extends Repository
{
    public function __construct(Order $order)
    {
        $this->model = $order;
    }

    public function create($data)
    {
//        DB::beginTransaction();
//        try {
            $order = $this->model->create($data['orderData']);
            $order->products()->createMany($data['productData']);
//            DB::commit();
            return $order;
//        } catch (\Illuminate\Database\QueryException $e) {
//            DB::rollback();
//            return false;
//        }
    }

}