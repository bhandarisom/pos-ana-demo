<?php
namespace App\Repositories;

use App\Models\ProductCategory;

class ProductCategoryRepository extends Repository
{
    public function __construct(ProductCategory $productCategory)
    {
        $this->model = $productCategory;
    }

    

     public function product_list_for_productEntry($parentId, $level, $selected)
    {
    	 $products = $this->model
            ->where('parent_id', $parentId)
            ->get();
        $returnList = '<select class="form-control" name="parent_id" data-live-search="true" id="parent_id">';
        $returnList .= '<option value="0" selected>Parent Itself</option>';
        foreach ($products as $product) {
                if ($selected > 0 && $selected == $product->id) {
                    $selected_option = 'selected="selected"';
                } else {
                    $selected_option = "";
                }

                $returnList .= '<option value="' . $product->id . '" ' . $selected_option . '>' .
                    str_repeat('&nbsp;>>&nbsp;', $level) . $product->title . '</option>';
                $returnList .= self::subproduct_list_for_productEntry($product->id, $level + 1, $selected);
        }
        $returnList .= '</select>';
        return $returnList;
    }
      public function subproduct_list_for_productEntry($parentId, $level, $selected = 0)
    {
        $subcategories = $this->model
            ->where('parent_id', $parentId)
            ->get();
        $returnList = '';
        foreach ($subcategories as $subproduct) {
                if ($selected > 0 && $selected == $subproduct->id) {
                    $selected_option = 'selected="selected"';
                } else {
                    $selected_option = "";
                }
                $returnList .= '<option value="' . $subproduct->id . '" ' . $selected_option . '>' .
                    str_repeat('&nbsp;>>&nbsp;', $level) . $subproduct->title . '</option>';
                $returnList .= self::subproduct_list_for_productEntry($subproduct->id, $level + 1, $selected);

        }
        return $returnList;
    }

      public function parent_list_for_productEntry($parentId, $level, $selected)
    {
        $products = $this->model
            ->where('parent_id', $parentId)
            ->orderby('id', 'asc')
            ->get();
        $returnList = '<select name="category_id" data-placeholder="Parent Itself/Select Products" class="form-control select-search" data-fouc>';
        // $returnList .= '<option value="category_id" selected>Parent Itself</option>';
        foreach ($products as $product) {
            if ($selected > 0 && $selected == $product->id) {
                $selected_option = 'selected="selected"';
            } else {
                $selected_option = "";
            }
            if(count($product->children) == 0){
            $returnList .= '<option value="' . $product->id . '" ' . $selected_option . '>'. $product->title . '</option>';
            }
            $returnList .= self::subparent_list_for_productEntry( $product->id, $level + 1, $selected, $product->title);


        }
        $returnList .= '</select>';
        return $returnList;
    }

    public function subparent_list_for_productEntry($parentId, $level, $selected = 0, $product_title = null)
    {
        $subcategories = $this->model
            ->where('parent_id', $parentId)
            ->orderby('id', 'desc')
            ->get();
        $returnList = '';
        foreach ($subcategories as $subproduct) {
            if ($selected > 0 && $selected == $subproduct->id) {
                $selected_option = 'selected="selected"';
            } else {
                $selected_option = "";
            }
         if(count($subproduct->children) == 0){
            $returnList .= '<option value="' . $subproduct->id . '" ' . $selected_option . '>'.$product_title.'>>'.$subproduct->title. '</option>';
         }  
            $returnList .= self::subparent_list_for_productEntry($subproduct->id, $level + 1, $selected,$product_title.'>>'.$subproduct->title);
        }
        return $returnList;
    }
}