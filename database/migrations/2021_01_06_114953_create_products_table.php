<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->unique();
            $table->double('amount', 8,2 )->nullable()->default(null);
            $table->string('slug')->nullable()->default(null)->unique();
            $table->string('attachment')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->string('product_code')->nullable()->default(null); 
            $table->double('previous_price', 8,2 )->nullable()->default(null);
            $table->text('dynamic_fields')->nullable()->default(null);
            $table->unsignedInteger('view_count')->default(0);
            $table->unsignedInteger('order_count')->default(0);
            $table->boolean('is_active')->default(0);
            $table->unsignedInteger('category_id');
            $table->foreign('category_id')
                ->references('id')
                ->on('product_categories')
                ->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
