<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number')->nullable()->default(null);
            $table->string('payment_id', 50)->nullable()->default(null);
            $table->text('notes')->nullable()->default(null);
            $table->string('coupon_code')->nullable()->default(null);
            $table->decimal('discount_amount', 15, 4)->nullable()->default(null);
            $table->decimal('sub_total', 15, 4)->nullable()->default(null);
            $table->decimal('shipping_charge', 15, 4)->nullable()->default(null);
            $table->decimal('total_price', 15, 4)->nullable()->default(null);
            $table->string('payment_status')->nullable()->default(null);
            $table->unsignedInteger('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
