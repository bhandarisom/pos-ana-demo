<?php

use Illuminate\Database\Seeder;

class ProductCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product_categories = [
            ['title'=>'Laptop','slug'=>'laptop','parent_id'=>0,'is_active'=>1],
            ['title'=>'Printer','slug'=>'printer','parent_id'=>0,'is_active'=>1],
            ['title'=>'Washing Machine','slug'=>'washing-machine','parent_id'=>0,'is_active'=>1],
        ];
        DB::table('product_categories')->insert($product_categories);
    }
}
