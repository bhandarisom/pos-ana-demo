<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            ['title'=>'Dell inspiron15','slug'=>'dell-inspiron15','previous_price'=>'80000', 'amount'=>'75000','is_active'=>1,'description'=>'i5 300 series hdmi wireless bluetooth waves audiomax','category_id'=>1],
            ['title'=>'Dell compact','slug'=>'dell-compact', 'previous_price'=>'55000','amount'=>'50000','is_active'=>1,'description'=>'i5 300 series hdmi wireless bluetooth waves audiomax','category_id'=>1],
            ['title'=>'Mac Book','slug'=>'mac-book', 'previous_price'=>'155000','amount'=>'150000','is_active'=>1,'description'=>'MacBook" is a term used for a brand of Mac notebook computers that Apple started producing in 2006.','category_id'=>1],
            ['title'=>'Mac Book Pro','slug'=>'mac-book-pro', 'previous_price'=>'115000','amount'=>'110000','is_active'=>1,'description'=>'MacBook" is a term used for a brand of Mac notebook computers that Apple started producing in 2006.','category_id'=>1],
            ['title'=>'Hp Pavillion','slug'=>'hp-pavillion', 'previous_price'=>'75000','amount'=>'70000','is_active'=>1,'description'=>'MacBook" is a term used for a brand of Mac notebook computers that Apple started producing in 2006.','category_id'=>1],
        ];

        DB::table('products')->insert($products);
    }
}
