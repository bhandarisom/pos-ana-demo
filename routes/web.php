<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

  Route::get('/', 'HomeController@index')->name('home.index');
  Route::get('productcategories/{slug?}', 'ProductCategoriesController@index')->name('productcategories.index');

  Route::post('select_product', 'ProductController@select_product')->name('product.select_product');
  Route::post('products/{product}/cart', 'CartController@store')->name('product.cart.update');
  Route::post('resetCart', 'CartController@resetCart')->name('product.cart.resetCart');
  Route::post('orderProducts', 'OrderController@store')->name('order.store');
  Route::get('orderPrint','OrderController@print')->name('order.print');
